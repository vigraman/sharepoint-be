const path = require('path')
const config = require('../config');
const fs = require('fs')
const jwt = require('jsonwebtoken')
authMiddleWare = (req, res, next) => {
    const token = req.headers.authentication.replace("Bearer ", "");
    const keyFile = path.join(__dirname, config.secret)
    const privateKey = fs.readFileSync(keyFile, 'utf8')
    jwt.verify(token, privateKey,  {algorithm: 'RS256'}, (err, decoded) =>  {
        console.log(err)
    })  
}

module.exports = authMiddleWare;


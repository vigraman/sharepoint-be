const jwt = require('jsonwebtoken');
const config = require('../config');
const path = require('path');
const fs = require('fs');

const generateToken = (data) => {
    const keyFile = path.join(__dirname, config.secret)
    const privateKey = fs.readFileSync(keyFile, 'utf8')
    const token =jwt.sign(data, privateKey, {algorithm: 'RS256'});
    return token
}

const generateResponse = (success, code, body) => {
    return {
        success: success,
        status: code,
        body: body
    }
}

module.exports = {
    generateToken,
    generateResponse
}
const loginService = require('../services/login.service')
const logger = require('../helper/logger').logger
class LoginController {
    login(data, res) {
        return loginService.login(data).then((docs) => {
            return docs
        }).catch((error) => {
            logger.error("Error in Login Controller:::",error)
            return error;
        });
    }
}

module.exports = new LoginController()
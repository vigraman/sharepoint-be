const fileService = require('../services/file.service')
const logger = require('../helper/logger').logger
class FileController {
    upload(data, res) {
        return fileService.upload(data).then((docs) => {
            return docs
        }).catch((error) => {
            logger.error("Error in File Controller:::",error)
            return error;
        });
    }
}

module.exports = new FileController()
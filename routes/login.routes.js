const router = require('express').Router();
const logger = require('../helper/logger').logger
class LoginRoutes {
    constructor(controller) {
        this.controller = controller;
        this.init();
        logger.info("Login Routes Initializing");
    }

    init() {
        router.use('/', async (req, res, next) => {
            next()
        });
        router.post("/login", async (req, res) => {
            try {                
                const resp = await this.controller.login(req.body, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error in Login Routes::::", error);
            }
        });
    }

    getRouter() {
        return router;
    }
}

module.exports = controller => new LoginRoutes(controller);
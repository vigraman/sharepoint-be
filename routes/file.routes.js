const router = require('express').Router();
const logger = require('../helper/logger').logger
const Authentication = require('../helper/authMiddleWare')
class FileRoutes {
    constructor(controller) {
        this.controller = controller;
        this.init();
        logger.info("File Routes Initializing");
    }

    init() {
        router.use('/', async (req, res, next) => {
            next()
        });
        router.post("/upload", async (req, res) => {
            try {                
                const resp = await this.controller.upload(req.body, res);
                res.send(resp);
            } catch(error) {
                logger.error("Error in File Routes::::", error);
            }
        });
    }

    getRouter() {
        return router;
    }
}

module.exports = controller => new FileRoutes(controller);
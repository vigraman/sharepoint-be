const db = require("./model");
const app = require("express")();
const bodyParser = require('body-parser');
const logger = require('./helper/logger').logger;
const cors = require('cors');
const port = require("./config").port

class Server {
    constructor() {
        this.init();
    }

    init() {
        this.initDB();
        this.initHttpServer();
        this.initControllers();
        this.initRoutes()
    }

    initDB() {
        db.connect();
    }

    initHttpServer() {
        app.use(bodyParser.json());
        app.use(cors());
        app.use(
            cors({
              orgin: [
                'http://localhost:3000/'
              ]
            })
        );
        app.listen(port, () => {
            logger.info("Listening in port::::", port)
        })
    }

    initControllers() {
        this.loginController = require("./controller/login.controller");
        this.fileController = require("./controller/file.controller");
    }

    initRoutes() {
        const loginRoutes = require('./routes/login.routes.js')(this.loginController);
        app.use('/api/user', loginRoutes.getRouter());

        const fileRoutes = require('./routes/file.routes.js')(this.fileController);
        app.use('/api/file', fileRoutes.getRouter());
    }
}

module.exports = new Server();
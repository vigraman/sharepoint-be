const loginModel = require('../model/login.model');
const logger = require('../helper/logger').logger
const generateToken = require('../helper/util').generateToken
const generateResponse = require('../helper/util').generateResponse
class LoginService {
    login(reqData) {
        return new Promise((resolve, reject) => {
            loginModel.find({email: reqData.email}).then((docs) => {
                if(docs.length) {
                    if(docs[0].password == reqData.password) {
                        const body = {
                            message: "SUCCESS",
                            accessToken: generateToken(reqData.email),
                        };
                        const response = generateResponse(true, 200, body);
                        resolve(response);
                    } else {
                        const body = {
                            message: "PWD_WRONG"
                        };
                        const response = generateResponse(false, 200, body);
                        resolve(response);
                    }
                } else {
                    const body = {
                        message: "USER_NOT_EXIST"
                    };
                    const response = generateResponse(false, 200, body);
                    resolve(response);
                }
            }).catch((error) => {
                if(error) {
                    console.log(error)
                    logger.error("Error in Login", error)
                    reject({ code: 402, message: 'ERROR' });
                }
            });
        })
    }
}

module.exports = new LoginService()
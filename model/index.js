const mongo = require("mongoose");
const config = require('../config/index').database;
const mongoUrl = `mongodb://${config.dbHost}:${config.dbPort}/${config.dbName}`;
const logger = require('../helper/logger').logger

connect = () => {
    mongo.connect(mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true}, (error) => {
        if(!error) {
            logger.info("Database Connection in the port::::", config.dbPort)
        } else {
            logger.error("Error in Database connection::::", e)
        }
    })    
}

module.exports.connect = connect;
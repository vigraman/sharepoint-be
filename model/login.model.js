const mongoose = require("mongoose");

var LoginSchema = new mongoose.Schema({
    email: {
        type: String,
        required: "Required"
    },
    password: {
        type: String,
        required: "Required"
    },
}, {timestamps: true})


module.exports = mongoose.model("user", LoginSchema);;

